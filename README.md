# SYS ADMIN #

The purpose of the present repo is to document how we
do all the Superuser stuff on a server that we have booted
to Ubuntu.

This is being tested on Ubuntu 18.04.5 LTS (bionic).

| Subject | File    |
| --------|---------|
| Setting up a user acccount for administration  | [`steps/00_admin_user.md`](https://bitbucket.org/voting-theory-forum/sys_adm_ubuntu/src/master/steps/00_admin_user.md)   |
| To integrate from other repo | [`steps/01_to_integrate.md`](https://bitbucket.org/voting-theory-forum/sys_adm_ubuntu/src/master/steps/01_to_integrate.md) |
| Installing, configuring, and testing Postfix for e-mail. | [`steps/03_postfix.md`](https://bitbucket.org/voting-theory-forum/sys_adm_ubuntu/src/master/steps/03_postfix.md) |
| Transport-layer Security | [`steps/tls.md`](https://bitbucket.org/voting-theory-forum/sys_adm_ubuntu/src/master/steps/tls.md) |

If something goes wrong in administration, it may be
addressed in [`when.md`](https://bitbucket.org/voting-theory-forum/sys_adm_ubuntu/src/master/when.md) .
