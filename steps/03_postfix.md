Postfix
=======

I give up on getting Haraka to work, so it's time to install
and configure _postfix_ to fulfill our
[MSA](https://en.wikipedia.org/wiki/Message_submission_agent)
needs.
```
sudo apt-get install -y postfix
```
It's giving me some broad overall configuration options.
I'm choosing "Internet site: Mail is sent and received 
directly using SMTP."

Hostname for mail: votingtheory.org

On installation, Postfix says:
```
setting myorigin
setting destinations: $myhostname, votingtheory.org, 
  localhost.org, , localhost
setting relayhost: 
setting mynetworks: 127.0.0.0/8 [::ffff:127.0.0.0]/104 
  [::1]/128
setting mailbox_size_limit: 0
setting recipient_delimiter: +
setting inet_interfaces: all
setting inet_protocols: all
/etc/aliases does not exist, creating it.
WARNING: /etc/aliases exists, but does not have a root 
  alias.

Postfix (main.cf) is now set up with a default 
  configuration.  If you need to make changes, edit 
  /etc/postfix/main.cf (and others) as needed.  To view
  Postfix configuration values, see postconf(1).

After modifying main.cf, be sure to run 'service postfix 
  reload'.
```
Want the `mail` command for testing.
```
sudo apt install -y mailutils
```
Changes to /etc/postfix/main.cf and /etc/aliases are in
the differences between trees c0 and c1 rooted at the top 
level of the present repo.

Checking Linode's
[Email Server Guides](https://www.linode.com/docs/email/).

The first of the server guides is
[Running a Mail Server](https://www.linode.com/docs/email/best-practices/running-a-mail-server/).
It reminds us of the need to set up an SPF record.
The example it gives is
```
example.com     86400   TXT     "v=spf1 a ~all"
```
and for more detail, the document refers to
[SPF_Record_Syntax](http://www.open-spf.org/SPF_Record_Syntax/).
I think will try 
"v=spf1 +ip4:198.58.123.207 +ip6:2600:3c00::f03c:92ff:fe7f:ad48 -all",
which says to allow outgoing mail from those addresses and 
exclude all others.
Some of these might not be necessary, but I am publishing:
```
TXT Record
Hostname	Value	TTL	
	
v=spf1 +ip4:198.58.123.207 +ip6:2600:3c00::f03c:92ff:fe7f:ad48 -all
	
5 minutes
	
mail
	
v=spf1 +ip4:198.58.123.207 +ip6:2600:3c00::f03c:92ff:fe7f:ad48 -all
	
5 minutes
	
mail.staging
	
v=spf1 +ip4:198.58.123.207 +ip6:2600:3c00::f03c:92ff:fe7f:ad48 -all
	
5 minutes
	
staging
	
v=spf1 +ip4:198.58.123.207 +ip6:2600:3c00::f03c:92ff:fe7f:ad48 -all
	
5 minutes
```
Submitted support request
to Linode, to get port 25 turned on, after
double checking SPF and A records and reverse DNS for
votingtheory.org,
our main domain. Received response saying they have turned
it on, and referring to several guides.

Still double-checking the portions of the config that I have
already drafted for Postfix, particularly the TLS keys.

Reminder from another page of this repo:
```
sudo certbot certificates
```
TLS key pointers are currently correct for the main domain.
The situation with the certs is a bit messy and at some
point, I should consolidate them and use a better name.
But I can put that off. I think it's not blocking the path 
to getting going with mail.

Now let's see what Linode has to say about configuring
Postfix (in general, even before checking the links in their
response). Searching with Google:
```
Linode Postfix
```
Found [Postfix](https://www.linode.com/docs/email/postfix/).
This document is just an index to documents specific to
operating systems, etc.  What Ubuntu are we running again?
```
lsb_release --all
```
Yes, it's 18.04 "bionic". Hmm, the index document does not
address this version. It has 10.04,10.10, 8.04, 9.04, 
and 9.10. The closest is 10.10. Leads to
[Basic Postfix Email Gateway on Ubuntu 10.10 (Maverick)](https://www.linode.com/docs/email/postfix/basic-postfix-email-gateway-on-ubuntu-10-10-maverick/). Well, that guide announces that it
is deprecated and is no longer being maintained.

The old doc recommends
```
home_mailbox = mail/
```
but I think that isn't necessary, as from some testing I
have already done, Postfix and the `mail` command seem to
work fine together with the mail going in
/var/mail/\[username\].
But what is this other suggestion?
```
virtual_alias_maps = hash:/etc/postfix/virtual
```
What do the Postfix docs say that that parameter means?
Consulting
[Postfix Configuration Parameters](http://www.postfix.org/postconf.5.html) (this document is avaliable over http and not 
https).
"The optional  virtual(5) alias table rewrites recipient 
addresses for all local, all virtual, and all  remote  mail  
destinations."
I believe this is unnecessary for us.

Let's double check the MX records.
```
dig -t mx votingtheory.org
```
It goes to mail.votingtheory.org, which has the same address
in its A record as votingtheory.org. I guess that is OK.

The rest of the doc from Linode doesn't seem to apply to us.

Let's turn to the references they sent in their response to
my support request.

The response starts out talking about rDNS and DNS more
generally, but I think I have that covered.

"Finally, we recommend you review Section 2 ("Abuse") of our 
[Acceptable Use Policy](https://www.linode.com/legal-aup/)."

OK, none of that is a problem for us.

Let's make sure the config files actually in use reflect
what I drafted in here, reload the server, and start sending
test e-mail messages.

```
set -u
repo=/home/sys/projects/sys_adm_ubuntu
cd $repo
find c1 -type f -print
f=etc/aliases
cmp /$f c1/$f
```
In this case, they are already equal.
```
f=etc/postfix/main.cf
cmp /$f c1/$f ||
  cat c1/$f | sudo tee /$f >/dev/null
```
Remind me what was the modern replacement for the old 
`service` command? I know I have used it, but I don't
seem to have mentioned it in my notes to date. Oh, yes,
it's *`systemctl`*!
```
systemctl status postfix
sudo systemctl reload postfix
```
Repeat the

```
systemctl status postfix
```
because it gives a snippet of log output, from which
you can
verify that Postfix didn't choke for any reason when trying 
to reload the config files.

Going to start a second window, tail the log file, and
send a test message.
```
cd /var/log
sudo tail -f mail.log
```
Other window.
```
ssh sys@votingtheory.org
set -u
mail aq29vu2quk@liamekaens.com
```
Log looks good!
```
Oct 15 06:13:14 votingtheory postfix/pickup[24631]: AA7151FF9F: uid=3 from=<sys@votingtheory.org>
Oct 15 06:13:14 votingtheory postfix/cleanup[25071]: AA7151FF9F: message-id=<20201015061314.AA7151FF9F@votingtheory.org>
Oct 15 06:13:14 votingtheory postfix/qmgr[24632]: AA7151FF9F: from=<sys@votingtheory.org>, size=364, nrcpt=1 (queue active)
Oct 15 06:13:15 votingtheory postfix/smtp[25073]: AA7151FF9F: to=<aq29vu2quk@liamekaens.com>, relay=mail.sneakemail.com[52.71.31.68]:25, delay=0.58, delays=0.02/0.01/0.3/0.25, dsn=2.0.0, status=sent (250 ok got it)
Oct 15 06:13:15 votingtheory postfix/qmgr[24632]: AA7151FF9F: removed
```
Postfix wouldn't have removed the message from the queue if
Postfix weren't convinced that Postfix successfully passed
the message off to mail.sneakemail.com.

Success! The message came through to me.

Trying direct to gmail. Not going to give my gmail address
here, because that's a secret!

Going to the other repo to talk about configuring NodeBB
with respect to mail. The job of the sys admin is done.

