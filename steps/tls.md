# Transport-layer Security #

## Approach of This Document ##

My (Jack Waugh's) original thought with writing these
documents was for them to support rebuilding everything
on a new server VM should that become necessary. And
indeed, that is one of the purposes of writing them.
But
structuring them as procedures for building from scratch
turns out not practical. The problem is that setting up the
server is a learning process and missteps are certain, and
given that the first priority timewise is getting the
server working rather than documenting how to do the next
one, it's not worth taking the time to test a set of
procedures that purport to tell the next integrater who
starts from scratch, exactly what to do.

So now I think the best thing to do is structure these
documents as a history of what I actually did to set up 
the server. If there is ever another iteration where
someone starts from scratch, they can use that opportunity
to try to smooth out some of the missteps and test better
procedures and document what they settle on.

## First Attempt ##

### Assessment of First Attempt ###

The first attept succeeded to demonstrate that it is
practical to set up TLS and use it for the Web.

In the case of mail, the mail config references the TLS
stuff. Whether that is necessary, I am not 100%
certain, but with the reference, it seems to work.

There are two problems with the first attempt. One is that
it is organizationally a mess. There are about two requests
for certs, and at least one of them is primarily named for
one of our secondary domains.

Also, I (Jack Waugh) think that when people come to our
primary website, they should see an introductory home page
instead of being dumped right into the discussion forum.
This gives us a place whence to link to our privacy policy
and terms of use and to the archive. So this consideration
leads me to think that the forum should be under
https://forum.votingtheory.org. So, we need the requests
for TLS certs to include that subdomain.

### History of the First Attempt ###

Guided by [link](https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx).

```
sudo apt-get remove certbot
sudo snap install --classic certbot
sudo certbot certonly --nginx
```

Where did the above procedure leave the relevant keys, that
I have to cite from the nginx config? The answer is:

```
/etc/letsencrypt
/etc/letsencrypt/live/voting-theory.com/fullchain.pem
```
There are account creds in there. We need a backup.
Only root can read all that, so in order to allow the
backup to be sucked to another box (e. g. in someones
home), need a copy that is not owned by root. It will
be owned by sys.
```
cd /etc/letsencrypt
sudo find . -print |
  sudo cpio -o >$HOME/etc_letsencrypt_bak.cpio
```
Copied that out onto home computer.

How do we configure nginx to use the "let's encrypt" keys?
Linking to [a guide](https://www.linode.com/docs/web-servers/nginx/enable-tls-on-nginx-for-https-connections/) for a clue.

Verified that nginx is compiled with the required option.

A major clue and breakthrough came by running this command:
```
sudo certbot certificates
```
as found in [this guide](https://certbot.eff.org/docs/using.html#nginx).

The configs in
c1/etc/nginx
demonstrate TLS.
Have the Superuser copy those into /etc/nginx/...
(corresponding subdirs) and put symbolic links in
/etc/nginx/sites-enabled pointing to corresponding names
in `/etc/nginx/sites-available` .

Coming back later to try to add more subdomains to the
TLS cert.
```
sudo certbot certonly --expand \
  -d staging.votingtheory.org \
  -d mail.votingtheory.org \
  -d mail.staging.votingtheory.org
```
I don't understand or like where it put the cert.
```
Saving debug log to /var/log/letsencrypt/letsencrypt.log

How would you like to authenticate with the ACME CA?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: Nginx Web Server plugin (nginx)
2: Spin up a temporary webserver (standalone)
3: Place files in webroot directory (webroot)
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate number [1-3] then [enter] (press 'c' to cancel): 2
Plugins selected: Authenticator standalone, Installer None
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for mail.staging.votingtheory.org
http-01 challenge for mail.votingtheory.org
http-01 challenge for staging.votingtheory.org
Waiting for verification...
Cleaning up challenges

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/staging.votingtheory.org/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/staging.votingtheory.org/privkey.pem
   Your cert will expire on 2021-01-09. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```
I thought it was going to expand the existing cert, but it
created a new one. I think the problem is that I didn't
specify the cert name. Let's revoke and delete both of them 
and create a new one with everything I think I may want.
Or maybe create the new one, then later after
configuring nginx and postfix to point to the new one,
delete the old ones and let them expire.

Reading in 
[using](https://certbot.eff.org/docs/using.html#re-creating-and-updating-existing-certificates),
I come to read it to say that the names are only for local
administration and have nothing to do with the cert's
content. But I still have one too many certs and would like
to just cite one from the configs (mail and web) and have
it apply to all the domains I could possibly want it to.

I could just expand one of them to include the other's
domains, but I think either name may confuse you.
So I will try to set everything straight.
```
sudo certbot certonly --standalone --cert-name all
```
Needs cleanup and consolidation.

## Addition of `forum` Subdomain; General Cleanup ##

Already added the `forum` subdomain to DNS, with an A
record giving the same address as the main domain's.

Running the command:
```
sudo certbot certificates
```
... shows that as a result of how the the First Attempt
(documented
above) went, the (local) names of the certs are 
voting-theory.com and staging.votingtheory.org, neither of 
which is our primary name and so neither of which makes me 
happy. Also, there is no valid reason to maintain two certs.

So, let's trash both of them.
```
set -u
pfx=/etc/letsencrypt/live
sfx=fullchain.pem
for n in staging.votingtheory.org voting-theory.com
do
  sudo ls -l $pfx/$n/$sfx
done
for n in staging.votingtheory.org voting-theory.com
do
  sudo certbot revoke \
    --cert-path $pfx/$n/$sfx \
    --reason superseded
done
```
The above command prompted for whether I wanted to delete
the files relating to the certificates I was revoking, and
in both cases, I answered in the affirmative.

I thought about bringing in a plugin to `certbot` that would
allow proof of control via DNS, but it would be too much
rigamarole. So I am falling back on the `standalone` method
for now. We may have to revisit this aspect for the periodic
renewal.
```
sudo systemctl stop nginx
sudo certbot certonly --standalone \
  --domain votingtheory.org \
  --domain forum.votingtheory.org \
  --domain forum.i4.votingtheory.org \
  --domain forum.i6.votingtheory.org \
  --domain i4.votingtheory.org \
  --domain i4.forum.votingtheory.org \
  --domain i6.votingtheory.org \
  --domain i6.forum.votingtheory.org \
  --domain mail.votingtheory.org \
  --domain mail.forum.votingtheory.org \
  --domain mail.staging.votingtheory.org \
  --domain staging.votingtheory.org \
  --domain www.votingtheory.org
```
That seemed to succeed. Part of the response:
```
   Your key file has been saved at:
   /etc/letsencrypt/live/votingtheory.org/privkey.pem
   Your cert will expire on 2021-03-16. To obtain a new or 
   tweaked version of this certificate in the future, simply 
   run certbot again. To non-interactively renew *all* of 
   your certificates, run "certbot renew"
```
Of course, we have to take those instructions with a grain
of salt.

Next thing to do is correct the `nginx` config for the 
website that serves our home page and the archive. Updated
`c1/etc/nginx/conf.d` accordingly.
```
set -u
f=etc/nginx/conf.d/tls.conf
cd /home/sys/projects/sys_adm_ubuntu
diff /$f c1/$f
sudo cp c1/$f /$f

sudo systemctl start nginx
```
Send a browser to `https://votingtheory.org/` (note,
*https*). That works. So, SSL is in place
for all sites hosted here. But we have to do something
about the renewal cycle by 2021-03. Also, before long, we
should make a backup of the keys (while still keeping them
secret).

What about mail?

Edited c1/etc/postfix/main.cf
```
cd /home/sys/projects/sys_adm_ubuntu
set -u
f=etc/postfix/main.cf
sudo cp c1/$f /$f
sudo systemctl reload postfix

echo "This is a test." |
  mail -s testing aq29vu2quk@liamekaens.com
```
See whether the mail comes through.

Nothing seems to be happening. Better check the log.
The mail log looks normal, and I don't see a bounce
message.

OK, it came through.

Left some domains off of the list above when I created
the cert. Now try to put them all into the existing cert.

```
sudo systemctl stop nginx
sudo certbot certonly --cert-name votingtheory.org \
  --domain votingtheory.org \
  --domain forum.votingtheory.org \
  --domain forum.i4.votingtheory.org \
  --domain forum.i6.votingtheory.org \
  --domain i4.votingtheory.org \
  --domain i4.forum.votingtheory.org \
  --domain i6.votingtheory.org \
  --domain i6.forum.votingtheory.org \
  --domain mail.votingtheory.org \
  --domain mail.forum.votingtheory.org \
  --domain mail.staging.votingtheory.org \
  --domain staging.votingtheory.org \
  --domain www.votingtheory.org \
  --domain votingtheory.com \
  --domain i4.votingtheory.com \
  --domain i6.votingtheory.com \
  --domain mail.votingtheory.com \
  --domain staging.votingtheory.com \
  --domain www.votingtheory.com
sudo systemctl start nginx
```

