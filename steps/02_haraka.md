Install, Configure, and Test Haraka
===================================

This project needed a
[mail submission agent](https://en.wikipedia.org/wiki/Message_submission_agent) (MSA). This file documents how
I tried
[Haraka](https://haraka.github.io/manual/tutorials/SettingUpOutbound.html) software for that purpose.
But I didn't like the way it was going, so switched
to Postfix.

So the rest of this file does not describe the current
configuration of the server. It is here for hysterical
porpoises or to support possible future change.

Haraka runs on _nodejs_, which is the main reason I wanted
to 
try Haraka instead of for example _postfix_ to fill the
MSA role.

Until someone tells me that diagnosing my problem requires
their knowing more about how I installed _nodejs_, I will
gloss over the details of that here. Actually it was done
with the help of a different user account on the server and 
what that account does is documented in a 
[different repo](https://bitbucket.org/voting-theory-forum/root/src/fbf37e1102b6470aff1b7284ad4a19bd91fbb524/setup_phases/00.txt?at=jw-006)
than the present one.

    node --version
    v12.18.3
    type node
    node is hashed (/home/sys/.asdf/shims/node)
    node
    Welcome to Node.js v12.18.3.
    Type ".help" for more information.
    > 3 + 4
    7
(Ctrl-D to exit)

From the above inputs and responses, we can see that
_nodejs_ is available and passes a rudimentary test.

I chose to 
[install Haraka](https://haraka.github.io/README.html) 
using _git_, because the alternative method that the manual 
offered looked more "global" than seemed necessary.
I'm installing in a user account that is not identically the
_root_ account but that can `sudo`.

    set -u
    cd
    mkdir haraka
    cd haraka
    git clone https://github.com/haraka/Haraka.git sw
    cd sw
    npm install
    haraka=`pwd`/bin/haraka

Now taking guidance from 
[SettingUpOutbound](https://haraka.github.io/manual/tutorials/SettingUpOutbound.html)
in the Manual.
```
cd ..
$haraka -i outbound
cd outbound
git init
git config user.email m2hh2kmhsn@snkmail.com
git config user.name "Jack WAUGH"
git add --all
git commit -m init
cd config
reconf=$HOME/projects/sys_adm_ubuntu/haraka/outbound/config
```
The _reconf_ variable points into a clone of
the present repo.
```
f=smtp.ini
cp -a $reconf/c1/$f .
```
Do you want to see the changes?
```
git diff $f
```
Here comes the output.
```
diff --git a/config/smtp.ini b/config/smtp.ini
index 042d57d..de86b55 100644
--- a/config/smtp.ini
+++ b/config/smtp.ini
@@ -5,18 +5,20 @@
 ; Note you can listen on multiple IPs/ports using commas:
 ;listen=127.0.0.1:2529,127.0.0.2:2529,127.0.0.3:2530
 
+listen=127.0.0.1:587
+
 ; public IP address (default: none)
 ; If your machine is behind a NAT, some plugins (SPF, GeoIP) gain features
 ; if they know the servers public IP. If 'stun' is installed, Haraka will
 ; try to figure it out. If that doesn't work, set it here.
-;public_ip=N.N.N.N
+public_ip=198.58.123.207
 
 ; Time in seconds to let sockets be idle with no activity
 ;inactivity_timeout=300
 
 ; Drop privileges to this user/group
-;user=smtp
-;group=smtp
+user=sys
+group=sys

 ; Don't stop Haraka if plugins fail to compile
 ;ignore_bad_plugins=0
@@ -25,18 +27,18 @@
 ;nodes=cpus
 
 ; Daemonize
-;daemonize=true
-;daemon_log_file=/var/log/haraka.log
-;daemon_pid_file=/var/run/haraka.pid
+daemonize=true
+daemon_log_file=/home/sys/run/log/haraka.log
+daemon_pid_file=/home/sys/run/pids/haraka.pid
 
 ; Spooling
 ; Save memory by spooling large messages to disk
-;spool_dir=/var/spool/haraka
+spool_dir=/home/sys/run/spool/haraka
 ; Specify -1 to never spool to disk
 ; Specify 0 to always spool to disk
 ; Otherwise specify a size in bytes, once reached the
 ; message will be spooled to disk to save memory.
-;spool_after=
+spool_after=512
 
 ; Force Shutdown Timeout
 ; - Haraka tries to close down gracefully, but if everything is shut down
```
More commands that I ran:
```
f=plugins
cp $f $reconf/c0 # Not idempotent -- don't repeat.
echo "tls
auth/flat_file" >$f
cp $f $reconf/c1
f=auth_flat_file.ini
cp -a $HOME/secrets/haraka/outbound/config/$f .
```
The file has different passwords, but otherwise look like
this:
```
[core]
methods=PLAIN,LOGIN,CRAM-MD5

[users]
theory=some_password
stage=some_other_password
```
Now I'm doing this:
```
cd ..
echo cd `pwd`
cd /home/sys/haraka/outbound
echo haraka=$haraka
haraka=/home/sys/haraka/sw/bin/haraka
sudo $haraka -c .
```
That produced some output (it's colorful, but I'm writing
in Markdown here, and there doesn't seem to be a way
to keep the colors):
```
loaded TLD files: 1=1508 2=8565 3=2448
loaded 8995 Public Suffixes
loglevel: INFO
log format: DEFAULT
[WARN] [-] [core] smtp.ini.nodes unset, using 1, see https://github.com/haraka/Haraka/wiki/Performance-Tuning
Starting up Haraka version 2.8.25
[INFO] [-] [core] Loading plugins
[INFO] [-] [core] Loading plugin: tls
[INFO] [-] [core] loading tls.ini
[ERROR] [-] [core] tls key /home/sys/haraka/outbound/config/tls_key.pem could not be loaded.
[ERROR] [-] [core] tls cert /home/sys/haraka/outbound/config/tls_cert.pem could not be loaded.
[INFO] [-] [core] Loading plugin: auth/flat_file
[NOTICE] [-] [core] Daemonizing...
```
I should probably supply the TLS keys where it is looking 
for them. But before doing that, need to make sure it is
shut down. Let's check the log to see whether Haraka stopped
by itself.
```
echo outbound=`pwd`
outbound=/home/sys/haraka/outbound
cd
ls
cd run; ls
cd log; ls
tail haraka.log
```
It has shut down, but with a crash, not a diagnostic.
```
[CRIT] [-] [core] Error: write EPIPE
[CRIT] [-] [core]     at process.target._send (internal/child_process.js:806:20)
[CRIT] [-] [core]     at process.target.send (internal/child_process.js:677:19)
[CRIT] [-] [core]     at sendHelper (internal/cluster/utils.js:26:15)
[CRIT] [-] [core]     at send (internal/cluster/child.js:193:10)
[CRIT] [-] [core]     at EventEmitter.cluster._setupWorker (internal/cluster/child.js:46:3)
[CRIT] [-] [core]     at initializeClusterIPC (internal/bootstrap/pre_execution.js:337:13)
[CRIT] [-] [core]     at prepareMainThreadExecution (internal/bootstrap/pre_execution.js:62:3)
[CRIT] [-] [core]     at internal/main/run_main_module.js:7:1
[NOTICE] [-] [core] Shutting down
```
So let's set up the TLS stuff to see whether the lack of
that is the sole cause of the crash.
```
echo cd `pwd`
cd /home/sys/run/log
```
Where did I put the "Let's Encrypt" state?
The relevant parts are in the config I'm using for _nginx_.
Will Haraka read them if they can only be read by the
Superuser?
For this first try, let's make local copies in userland, to
minimize the number of variables for purposes of diagnosis.
```
cd /home/sys/haraka/outbound/config
sudo cat \
  /etc/letsencrypt/live/voting-theory.com/fullchain.pem \
  >tls_cert.pem
sudo cat \
  /etc/letsencrypt/live/voting-theory.com/privkey.pem \
  >tls_key.pem
cd /home/sys/run/log
ls
rm -f haraka.log
cd /home/sys/haraka/outbound
echo haraka=$haraka
haraka=/home/sys/haraka/sw/bin/haraka
sudo $haraka -c .
loaded TLD files: 1=1508 2=8565 3=2448
loaded 8995 Public Suffixes
loglevel: INFO
log format: DEFAULT
[WARN] [-] [core] smtp.ini.nodes unset, using 1, see https://github.com/haraka/Haraka/wiki/Performance-Tuning
Starting up Haraka version 2.8.25
[INFO] [-] [core] Loading plugins
[INFO] [-] [core] Loading plugin: tls
[INFO] [-] [core] loading tls.ini
[INFO] [-] [core] Generating a 2048 bit dhparams file at /home/sys/haraka/outbound/config/dhparams.pem
[INFO] [-] [core] Loading plugin: auth/flat_file
[NOTICE] [-] [core] Daemonizing...

cd /home/sys/run/log
cat *
[INFO] [-] [core] Loading plugins
[INFO] [-] [core] Loading plugin: tls
[INFO] [-] [core] loading tls.ini
[INFO] [-] [core] Loading plugin: auth/flat_file
[CRIT] [-] [core] Error: write EPIPE
[CRIT] [-] [core]     at process.target._send (internal/child_process.js:806:20)
[CRIT] [-] [core]     at process.target.send (internal/child_process.js:677:19)
[CRIT] [-] [core]     at sendHelper (internal/cluster/utils.js:26:15)
[CRIT] [-] [core]     at send (internal/cluster/child.js:193:10)
[CRIT] [-] [core]     at EventEmitter.cluster._setupWorker (internal/cluster/child.js:46:3)
[CRIT] [-] [core]     at initializeClusterIPC (internal/bootstrap/pre_execution.js:337:13)
[CRIT] [-] [core]     at prepareMainThreadExecution (internal/bootstrap/pre_execution.js:62:3)
[CRIT] [-] [core]     at internal/main/run_main_module.js:7:1
[NOTICE] [-] [core] Shutting down
```

On reflection after abandoning this branch, I suspect that
the reason for the crash was that port 25 was
blocked by Linode. However, it wasn't a good sign, that
Haraka crashed instead of diagnosing the problem. Even
worse was that I couldn't contact any community that could
help. Their docs about their community gave two methods
of contact: IRC on freenode, and a mailing list. The
mailing list doesn't seem to be active, and freenode
won't take a disposable e-mail address anymore.

