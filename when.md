# WHEN...

---

*When* you try to update the system software and it gives you
that "held back" guff:

[guide](https://www.google.com/search?channel=fs&client=ubuntu&q=apt-get+kept+back)
```
sudo apt-get update
sudo apt-get --with-new-pkgs upgrade
```

---

*When* the system has been restarted, but the websites
have not, (maybe because you haven't automated that):

You have to start Mongo first, followed by the nodebb
instances.
```
sudo systemctl start mongod
sudo su -l theory -c 'cd; cd nodebb; ./nodebb start'
```
You may have to start up one or two nodejs applications for
the home page and the archive. For now, the way is to log
in as "theory" and visit those one at a time. You can think
about rotating their logs while you are there. But it's
going to go, more or less, along these lines:
```
sudo su -l theory -c \
'cd; cd projects/archive; setsid node index.js >log.log 2>&1'
sudo su -l theory -c \
'cd; cd projects/homepg; setsid node index.js >log.log 2>&1'


```
