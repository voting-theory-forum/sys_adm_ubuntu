# SET UP A LOGIN FOR DOING SYS ADMIN STUFF #

The bias of the person starting this is not to be logged in
as ```root``` any more than necessary. I like to use a user
account for doing superuser stuff. I want to support a
version of ```su``` in said account.

Among the "system" accounts already present in the system as
installed, the ```sys``` user looks possibly appropriatable
for this purpose (in the past, I have used ```adm```, but
it isn't present in this system, and why not use what we
already have?).

```
usermod --home /home/sys --shell /bin/bash sys
cd /home
umask 2
mkdir sys
chown sys sys
chgrp sys sys
cp -ar /root/.ssh sys
chown sys `find sys/.ssh -print`
chgrp sys `find sys/.ssh -print`
```

At this point, it should be possible to log in as sys.
Suggesting you do so in a separate window; keep the root
window around to return to later.

```
ssh sys@votingtheory.com
set -u
umask # is 2
echo "$PATH" | tee origpath
ls -al
```

There is no .bashrc nor .profile, .bash_login, etc.
Are we fine with that?

```
mkdir projects
cd projects
git clone \
  git@bitbucket.org:voting-theory-forum/sys_adm_ubuntu.git
cd sys_adm_ubuntu
git config user.email m2hh2kmhsn@snkmail.com
git config user.name "Jack WAUGH"
cd src
make
```

Now go back to your superuser window.

```
cd /home/sys/gate
chown root sudo
chgrp root sudo
chmod g+s,u+s sudo
ls -l sudo
```

Test:

Return to your window that is logged in as "sys".

Log out, then log in again.

```
sudo id
```

If it says root, congratulations. If not, call 
Houston,
because we've had a problem.




