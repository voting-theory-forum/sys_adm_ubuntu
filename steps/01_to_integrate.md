# To Integrate

If we move all the system stuff from the 
[other repos](https://bitbucket.org/voting-theory-forum/workspace/projects/VF)
to
the  present repo, snippets from the present file can go
where they belong in the middles of the resulting files.
Or in some cases take the place of old versions.

```
sudo hostnamectl set-hostname votingtheory.org
```

## Setting Up User Accounts

After they are set up by the current steps or equivalent
(made idempotent?), add these actions:

```
sudo adduser sys adm
sudo adduser sys share
sudo adduser sys stage
sudo adduser sys theory
sudo adduser share stage
sudo adduser share theory
```

